/**
 * @file main.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Example application to control the real BAZAR robot using the cooperative task space formalism
 * @date 13-03-2020
 * License: CeCILL
 */
#include <rkcl/robots/bazar.h>
#include <rkcl/processors/otg_reflexxes.h>
#include <rkcl/processors/app_utility.h>
#include <yaml-cpp/yaml.h>
#include <pid/rpath.h>
#include <pid/signal_manager.h>

#include <iostream>
#include <vector>
#include <thread>
#include <unistd.h>
#include <mutex>

int main()
{
    rkcl::DriverFactory::add<rkcl::KukaLWRFRIDriver>("fri");
    rkcl::DriverFactory::add<rkcl::NeobotixMPO700Driver>("neobotix_mpo700");

    rkcl::QPSolverFactory::add<rkcl::OSQPSolver>("osqp");

    auto conf = YAML::LoadFile(PID_PATH("bazar_config/bazar_one_arm_simu_init_config.yaml"));
    auto app = rkcl::AppUtility::create<rkcl::ForwardKinematicsRBDyn, rkcl::JointSpaceOTGReflexxes>(conf);
    app.add<rkcl::CollisionAvoidanceSCH>();

    rkcl::TaskSpaceOTGReflexxes task_space_otg(app.getRobot(), app.getTaskSpaceController().getControlTimeStep());
    rkcl::DualATIForceSensorDriver dual_force_sensor_driver(app.getRobot(), conf["force_sensor_driver"]);
    rkcl::CooperativeTaskAdapter coop_task_adapter(app.getRobot(), conf["cooperative_task_adapter"]);

    std::vector<rkcl::PointWrenchEstimator> point_wrench_estimators;
    if (conf["point_wrench_estimators"])
    {
        for (const auto& wrench_estimator : conf["point_wrench_estimators"])
            point_wrench_estimators.push_back(rkcl::PointWrenchEstimator(app.getRobot(), wrench_estimator));
    }

    dual_force_sensor_driver.init();

    if (not app.init())
    {
        throw std::runtime_error("Cannot initialize the application");
    }

    app.addDefaultLogging();

    bool stop = false;
    bool done = false;

    pid::SignalManager::registerCallback(pid::SignalManager::Interrupt, "stop", [&stop](int) { stop = true; });
    // pid::SignalManager::registerCallback(pid::SignalManager::UserDefined1, "next", [&done](int){ done = true; });

    try
    {
        std::cout << "Starting control loop \n";
        app.configureTask(0);
        task_space_otg.reset();
        while (not stop and not done)
        {
            bool ok = app.runControlLoop(
                [&] {
                    if (app.isTaskSpaceControlEnabled())
                    {
                        bool ok = dual_force_sensor_driver();

                        for (auto& wrench_estimator : point_wrench_estimators)
                            ok &= wrench_estimator();

                        ok &= coop_task_adapter();
                        ok &= task_space_otg();
                        return ok;
                    }
                    else
                        return true;
                });

            if (ok)
            {
                done = true;
                if (app.isTaskSpaceControlEnabled())
                {
                    done &= (app.getTaskSpaceController().getControlPointsPoseErrorGoalNormPosition() < 0.01 || (app.getTaskSpaceController().getControlPointsForceErrorTargetNorm() < 5 && app.getTaskSpaceController().useDamping()));
                }
                if (app.isJointSpaceControlEnabled())
                {
                    for (const auto& joint_space_otg : app.getJointSpaceOTGs())
                    {
                        if (joint_space_otg->getJointGroup()->control_space == rkcl::JointGroup::ControlSpace::JointSpace)
                        {
                            if (joint_space_otg->getControlMode() == rkcl::JointSpaceOTG::ControlMode::Position)
                            {
                                auto joint_group_error_pos_goal = joint_space_otg->getJointGroup()->selection_matrix * (joint_space_otg->getJointGroup()->goal.position - joint_space_otg->getJointGroup()->state.position);
                                done &= (joint_group_error_pos_goal.norm() < 0.001);
                            }
                            else if (joint_space_otg->getControlMode() == rkcl::JointSpaceOTG::ControlMode::Velocity)
                            {
                                auto joint_group_error_vel_goal = joint_space_otg->getJointGroup()->selection_matrix * (joint_space_otg->getJointGroup()->goal.velocity - joint_space_otg->getJointGroup()->state.velocity);
                                done &= (joint_group_error_vel_goal.norm() < 1e-10);
                            }
                        }
                    }
                }
            }
            else
            {
                throw std::runtime_error("Something wrong happened in the control loop, aborting");
            }
            if (done)
            {
                done = false;
                std::cout << "Task completed, moving to the next one" << std::endl;
                done = not app.nextTask();
                task_space_otg.reset();
            }
        }
        if (stop)
            throw std::runtime_error("Caught user interruption, aborting");

        std::cout << "All tasks completed" << std::endl;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    pid::SignalManager::unregisterCallback(pid::SignalManager::Interrupt, "stop");

    std::cout << "Ending the application" << std::endl;

    if (app.end())
        std::cout << "Application ended properly" << std::endl;
    else
        std::cout << "Application ended badly" << std::endl;
    return 0;
}
